# Bestie ChatBot 

A web app implementation of simple chatbot that assists, guides, and also engages with students during their free time. It gives the feel of always staying with your bestie (friend). The bot is built using the chatterbot library in python and web app is built using flask. This web app is deployed using free hosting(pythonanywhere) [Take a look at the web app : [Bestie chatbot](http://kaushik.pythonanywhere.com/)]

# Local Setup:

1. Ensure that Python, Flask, SQLAlchemy, Google and ChatterBot are installed (either manually, or run `pip install -r requirements.txt`).
2. Run bot.py with `python bot.py`.