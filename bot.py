from flask import Flask, render_template, request
from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer
from googlesearch import search

app = Flask(__name__)

bot = ChatBot("Nareka", storage_adapters="chatterbot.storage.SQLStorageAdapter",logic_adapters = [
    "chatterbot.logic.BestMatch",
    "chatterbot.logic.MathematicalEvaluation"])

bot_trainer = ChatterBotCorpusTrainer(bot, show_training_progress=False)

bot_trainer.train("data/dataset.yml")
bot_trainer.train('chatterbot.corpus.english')

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/get")
def get_response():
    flag=0
    userText = request.args.get('msg')

    botReply = str(bot.get_response(userText))

    if(botReply == 'help in list.'):
        query = "best project using latest technology"
        flag=1

    if(botReply == 'help in grades.'):
        query = "how to improve on grades?"
        flag=1

    if(flag == 1):
        links = ""
        for j in search(query, tld="co.in", num=10, stop=10, pause=2):
            links += j + "\n"
        return links
    else:
        return botReply

if __name__ == '__main__':
    app.run(debug = False)